var request = require('request');
var express = require('express');
var bodyParser = require('body-parser');
var app = express();
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true}));

app.post('/getPeople', function(req, res){
  let action = req.body.queryResult.action;
  if(action == 'getPeople'){
      var url = 'https://reqres.in/api/login';
      var obj = {
        "email": "sydney@fife",
        "password": "pistol"
      }

      var options = {
        method: 'post',
        body: obj,
        json: true,
        url: url
      }

        request(options, function(err,httpResponse,body){ 
          if(err){
            return res.json({
              "fulfillmentText": "Sorry! can you please come again.",
              "fulfillmentMessages": [
                {"simpleResponses": 
                    {"simpleResponses": [
                        {
                            "textToSpeech": "Sorry! can you please come again.",
                            "displayText": "Sorry! can you please come again."
                        }
                      ]
                    }
                }
              ],
              source: ""
            })
          } else if(httpResponse){
            return res.json({
                "fulfillmentText": "Person who uses it is Shishir and the admin id is "+httpResponse.body.token,
                "fulfillmentMessages": [
                  {"simpleResponses": 
                      {"simpleResponses": [
                          {
                              "textToSpeech": "Person who uses it is Shishir and the admin id is "+httpResponse.body.token,
                              "displayText": "Person who uses it is Shishir and the admin id is "+httpResponse.body.token
                          }
                        ]
                      }
                  }
                ],
                source: ""
            })
          }else{
            return res.json({
              "fulfillmentText": "Sorry! can you please come again.",
              "fulfillmentMessages": [
                {"simpleResponses": 
                    {"simpleResponses": [
                        {
                            "textToSpeech": "Sorry! can you please come again.",
                            "displayText": "Sorry! can you please come again."
                        }
                      ]
                    }
                }
              ],
              source: ""
            })
          }
      })
  }

  if(action == 'addPeople'){
    var url = 'https://reqres.in/api/users';
  
    var obj = {
      "name": req.body.queryResult.parameters.userName,
      "job": req.body.queryResult.parameters.jobRole
    }
  
    var options = {
      method: 'post',
      body: obj,
      json: true,
      url: url
    }
  
    request(options, function(err,httpResponse,body){
      if(err){
        return res.json({
          "fulfillmentText": "Sorry! can you please come again.",
          "fulfillmentMessages": [
            {"simpleResponses": 
                {"simpleResponses": [
                    {
                        "textToSpeech": "Sorry! can you please come again.",
                        "displayText": "Sorry! can you please come again."
                    }
                  ]
                }
            }
          ],
          source: ""
        })
      } else if(httpResponse){
        return res.json({
            "fulfillmentText": "The user is created with User Name "+httpResponse.body.name+", job role as "+httpResponse.body.job+" and its User Identity number is "+httpResponse.body.id,
            "fulfillmentMessages": [
              {"simpleResponses": 
                  {"simpleResponses": [
                      {
                          "textToSpeech": "The user is created with User Name "+httpResponse.body.name+", job role as "+httpResponse.body.job+" and its User Identity number is "+httpResponse.body.id
                      }
                    ]
                  }
              }
            ],
            source: ""
        })
      }else {
        return res.json({
          "fulfillmentText": "Sorry! can you please come again.",
          "fulfillmentMessages": [
            {"simpleResponses": 
                {"simpleResponses": [
                    {
                        "textToSpeech": "Sorry! can you please come again.",
                        "displayText": "Sorry! can you please come again."
                    }
                  ]
                }
            }
          ],
          source: ""
        })
      }
    })
  }
})

app.listen(process.env.PORT || 5000);
console.log('App running at 5000');